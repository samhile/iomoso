IoMoSo - Ion Molmer Soreson Gate Simulations

Copyright (c) 2018 Chris Knapp, Sam Hile, Simon Webster
All rights reserved.

A package designed to make it as easy as possible to simulate complex Hamiltonians representing motionally entangled trapped ions. 
The library contains a core class defining a Hamiltonian object. Methods may be called on this object to add microwave or radio-frequency field terms. Each term may use either scalar or array type Parameters, and the result computed simply over time, or over an array of varied parameter values. 
The initial state of the system is then defined, and the Hamiltonian solved for the given Parameters, returning the results of the integration at each timestep as a new object that can be analysed.

More readme coming soon...?