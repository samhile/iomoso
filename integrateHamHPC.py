#!/usr/bin/python
import os
import sys
import qutip as qu
import pickle

try:
  env_variable = os.environ['SGE_TASK_ID']
except KeyError:
  print('No environment variable set')
else:
  print(env_variable)

file = open('mepars_{job_no}.pickle'.format(job_no = env_variable), 'rb')
mePars = pickle.load(file)
file.close()

options = qu.Options(atol=1e-9, rtol=1e-9, nsteps=60000)
result = qu.mesolve(mePars['H'], mePars['initial'], mePars['times'], [], [], options=options)

file = open('meresult_{job_no}.pickle'.format(job_no = env_variable), 'wb')
mePars = pickle.dump(result, file)
file.close()
