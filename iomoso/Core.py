#!/usr/bin/env
import qutip as qu
import numpy as np
import pickle
import os
from numpy import nan, isnan
from functools import partial
from sys import exit




class Hamiltonian:
    def __init__(self, nions, nfock, gradient = None, ZeeSO = None, eta1 = None, mode = 'com', trapfreq = None):
        """
        The core MS Hamiltonian class, used to prepare a qutip hamiltonian...
        blab...
        """
        self.nions = nions
        self.N = nfock
        self.trapfreq = trapfreq
        self.eta1 = eta1
        self.zshift = ZeeSO
        self.gradient = gradient

        self.Imot = qu.qeye(self.N)
        self.Ispin = qu.qeye(4)
        self.a = qu.tensor(*([self.Ispin]*self.nions + [qu.destroy(self.N)]))
        #self.lastN = qu.tensor(*([self.Ispin]*self.nions + [qu.basis(self.N, self.N - 1)]))

        self.s0 = qu.basis(4,3)   # "0" state                 
        self.s1 = qu.basis(4,2)   # "-1" state
        self.s0p = qu.basis(4,1)  # "0'" state 
        self.s2 = qu.basis(4,0)   # "+1" state
        self.u = 0.5*self.s2 + 0.5*self.s1 + (1/np.sqrt(2))*self.s0
        self.d = 0.5*self.s2 + 0.5*self.s1 - (1/np.sqrt(2))*self.s0
        self.D = (1/np.sqrt(2))*(self.s2 - self.s1)
        self.states_dict = {'0':self.s0, '-1':self.s1, '0p':self.s0p, '+1':self.s2, 'u':self.u, 'd':self.d, 'D':self.D, 'none':self.Ispin}
        # dictionaries containing all of the states 
        self.bell_dic = { 'bell1':1/np.sqrt(2)*((qu.tensor(self.D, self.D, self.Imot) + qu.tensor(self.s0p, self.s0p, self.Imot))),
                          'bell2':1/np.sqrt(2)*((qu.tensor(self.D, self.D, self.Imot) - qu.tensor(self.s0p, self.s0p, self.Imot))),
                          'bell3':1/np.sqrt(2)*((qu.tensor(self.D, self.D, self.Imot) + 1j*qu.tensor(self.s0p, self.s0p, self.Imot))),
                          'bell4':1/np.sqrt(2)*((qu.tensor(self.D, self.D, self.Imot) - 1j*qu.tensor(self.s0p, self.s0p, self.Imot))) }

        self.Hterms = [] # list that contains all of the Hamiltonian terms 

        oscillator = {'op':self.trapfreq*self.a.dag()*self.a, 'timedep':False}
        self.Hterms.append(oscillator) 
        #no hyperfine term?
        if gradient:
            """
            if gradient=True, define the shift due to the gradient and define the sigmaz operator. Then,
            iterate over the number of ions in the system and for each ion and generate a sigmaz operator
            using the op_construct function (see that function for details on how it works). These sigmaz
            operators are stored in a list, of which the sum is then taken giving the COM mode (not the 
            stretch mode as stated, I think). sigma_stretch is then an operator containing all of the 
            sigmaz operators for each atom, which is then assigned to the 'op' key in the dictionary test1.
            The test 2 dictionary is just the number operator a(dag).a. These dictionaries are then added
            to the Hterms list, which contains all of the Hamiltonian terms
            """
            self.dgrad = self.trapfreq*(self.eta1**2) #*0.5 ?  (the energy shift at z_0\eta displacement)
            self.sigmaz = self.s2*self.s2.dag() - self.s1*self.s1.dag() 
            sigmaz_all = sum([self.op_construct(i+1, self.sigmaz) for i in range(self.nions)]) # i+1 as op_construct does ion - 1
            coupling = {'op':self.trapfreq*self.eta1*sigmaz_all*(self.a + self.a.dag()), 'timedep':False} #is this off by a half? #*0.5 ?
            self.Hterms.append(coupling)
            #no electron Zeeman terms? - detuning applied to all drive fields??? in AddXXplus/minus functions
              
        else:                         
            self.dgrad = 0 # if gradient=False in Hamiltonian object, set shift due to gradient = 0

    def initial_state(self, initial_state_list, motional_state):   # takes a list of strings as arguments and a motional state
        """
        function defining the initial state of the system, takes a list of strings and motional state as 
        arguments. It first checks if the list is the same length as the number of ions, if this is true 
        the state corresponding to the string is found in the states dictionary and then added to a list.
        Finally, a term describing the motional state is added to the end of the list and then all values
        in the list are tensored together.
        """
        if len(initial_state_list) == self.nions: 
            initial_state_list = [ self.states_dict[i] for i in initial_state_list ]
            return qu.tensor(*(initial_state_list + [qu.coherent(self.N, motional_state)]))
        else:
            raise ValueError('Number of initial states specified in list argument is '
                             'different to the number of ions the Hamiltonian object was '
                             'initialised with. Please ensure the initial states of all ions '
                             'are declared.')

    def op_construct(self, ion, transition):
        """
        This function is used to construct terms describing transitions between levels on certain ions.
        We first create a list that contains n Ispin elements (where here n is the number of ions) which
        represent all of the ions in the system. A term describing the motion is then appended to the end
        of the list. We then replace an Ispin element of the list with the transition the op_construct
        function took as an argument (e.g. from addUwPlus this would be self.s2*self.s0.dag()), at the 
        position in the list that corresponds to the ion that the transition takes place on. E.g. for 3
        ions, we would have base = [Ispin, Ispin, Ispin, Imot], and if we want to add a transition between
        the 0 and +1 states on the second ion using the addUwPlus transition, the list would become
        [Ispin, self.s2*self.s0.dag(), Ispin, Imot]. All of these elements would then be tensored together
        to give the final operator.
        """
        base = [self.Ispin]*self.nions + [self.Imot]
        base[ion - 1] = transition
        return qu.tensor(*base)

    def addUwPlus(self, ion, amp, det):
        """
        All of these functions have the same form so this will be the only one with a docstring. We start
        by calling the op_construct function to construct the operator associated with the transition. 
        the string in factor1 allows for the time-dependent string (has to be a string to allow QuTiP to
        use cython) to take amp (Rabi freq), det (detuning), and dgrad (shift due to gradient) as arguments.
        The operator, the time-dependent part (factor1) and the parameters are then assigned to their
        respective keys in the dictionary Hterm1. This dictionary is then added to the Hterms list. This
        whole process is then repeated for the conjugate term (factor2, Hterm2).
        """
        op = self.op_construct(ion, self.s2*self.s0.dag()) 
        factor1 = partial('0.5*{amp}*exp(1j*({det}+{dgrad})*t)'.format, dgrad = self.dgrad)
        Hterm1 = {'op':op, 'factor': factor1, 'amp': amp, 'det': det, 'timedep':True}
        op = self.op_construct(ion, self.s0*self.s2.dag())
        factor2 = partial('0.5*{amp}*exp(-1j*({det}+{dgrad})*t)'.format, dgrad = self.dgrad)
        Hterm2 = {'op':op, 'factor': factor2, 'amp': amp, 'det': det, 'timedep':True}
        self.Hterms.append(Hterm1)
        self.Hterms.append(Hterm2)

    def addUwMinus(self, ion, amp, det):
        op = self.op_construct(ion, self.s1*self.s0.dag())
        factor1 = partial('0.5*{amp}*exp(1j*({det}+{dgrad})*t)'.format, dgrad = self.dgrad)
        Hterm1 = {'op':op, 'factor':factor1, 'amp':amp, 'det':det, 'timedep':True}
        op = self.op_construct(ion, self.s0*self.s1.dag()) 
        factor2 = partial('0.5*{amp}*exp(-1j*({det}+{dgrad})*t)'.format, dgrad = self.dgrad)
        Hterm2 = {'op':op, 'factor':factor2, 'amp':amp, 'det':det, 'timedep':True}
        self.Hterms.append(Hterm1)
        self.Hterms.append(Hterm2)

    def addRfPlus(self, ion, amp, det, includeMinus = True):
        """
        The addRfPlus and addRfMinus functions also have the capability to account for the Second
        order Zeeman shift by including the terms corresponding to transition that isn't being
        driven.
        """
        op = self.op_construct(ion, self.s2*self.s0p.dag()) 
        factor1 = partial('0.5*{amp}*exp(1j*({det}+{dgrad})*t)'.format, dgrad = self.dgrad)
        Hterm1 = {'op':op, 'factor': factor1, 'amp':amp, 'det': det, 'timedep':True}
        op = self.op_construct(ion, self.s0p*self.s2.dag()) 
        factor2 = partial('0.5*{amp}*exp(-1j*({det}+{dgrad})*t)'.format, dgrad = self.dgrad)
        Hterm2 = {'op':op, 'factor': factor2, 'amp':amp, 'det': det, 'timedep':True}
        self.Hterms.append(Hterm1)
        self.Hterms.append(Hterm2)
        if includeMinus:
            op = self.op_construct(ion, self.s0p*self.s1.dag()) 
            factor3 = partial('1*0.5*{amp}*exp(1j*({det}+{dgrad})*t)'.format, dgrad = self.dgrad)
            Hterm3 = {'op':op, 'factor':factor1, 'amp':amp, 'det':det + self.zshift[ion - 1], 'timedep':True}
            op = self.op_construct(ion, self.s1*self.s0p.dag())
            factor4 = partial('1*0.5*{amp}*exp(-1j*({det}+{dgrad})*t)'.format, dgrad = self.dgrad)
            Hterm4 = {'op':op, 'factor':factor2, 'amp':amp, 'det':det + self.zshift[ion - 1], 'timedep':True}
            self.Hterms.append(Hterm3)
            self.Hterms.append(Hterm4)

    def addRfMinus(self, ion, amp, det, includePlus = True):
        op = self.op_construct(ion, self.s0p*self.s1.dag()) 
        factor1 = partial('0.5*{amp}*exp(1j*({det}+{dgrad})*t)'.format, dgrad = -self.dgrad)
        Hterm1 = {'op':op, 'factor':factor1, 'amp':amp, 'det':det, 'timedep':True}
        op = self.op_construct(ion, self.s1*self.s0p.dag())
        factor2 = partial('0.5*{amp}*exp(-1j*({det}+{dgrad})*t)'.format, dgrad = -self.dgrad)
        Hterm2 = {'op':op, 'factor':factor2, 'amp':amp, 'det':det, 'timedep':True}
        self.Hterms.append(Hterm1)
        self.Hterms.append(Hterm2)
        if includePlus:
            op = self.op_construct(ion, self.s2*self.s0p.dag()) 
            factor3 = partial('0.5*{amp}*exp(1j*({det}+{dgrad})*t)'.format, dgrad = self.dgrad)
            Hterm3 = {'op':op, 'factor': factor1, 'amp':amp, 'det':det - self.zshift[ion - 1], 'timedep':True}
            op = self.op_construct(ion, self.s0p*self.s2.dag()) 
            factor4 = partial('0.5*{amp}*exp(-1j*({det}+{dgrad})*t)'.format, dgrad = self.dgrad)
            Hterm4 = {'op':op, 'factor': factor2, 'amp':amp, 'det':det - self.zshift[ion - 1], 'timedep':True}
            self.Hterms.append(Hterm3)
            self.Hterms.append(Hterm4)

    def addClockTrans(self, ion, amp, det):  # 0 --> 0' transition
        op = self.op_construct(ion, self.s0*self.s0p.dag())
        factor1 = partial('0.5*{amp}*exp(1j*({det}+{dgrad})*t)'.format, dgrad = self.dgrad) #hmm, don't think we need dgrad here
        Hterm1 = {'op':op, 'factor':factor1, 'amp':amp, 'det':det, 'timedep':True}
        op = self.op_construct(ion, self.s0p*self.s0.dag())
        factor2 = partial('0.5*{amp}*exp(-1j*({det}+{dgrad})*t)'.format, dgrad = self.dgrad)
        Hterm2 = {'op':op, 'factor': factor2, 'amp': amp, 'det': det, 'timedep':True}
        self.Hterms.append(Hterm1)
        self.Hterms.append(Hterm2)


        
class Parameter:
    def __init__(self, start, stop = nan, default = nan, includeFinal = True):
        '''
        Class defining a parameter range. Range limits are set at creation, number of points within (and therefore resolution) are selected at use via an argument to the getValues(numValues) method.
        '''
        self.start = start
        self.stop = stop
        self.default = default
        self.includeFinal = includeFinal
    
    def getValues(self, numValues):
        return np.linspace(self.start, self.stop, numValues, self.includeFinal) # returns array of parameter object values 

    def getValue(self, i = None, numValues = None):
        if i is not None:                                                # if a value of i is given, return the value in the list
            return self.start + (self.stop - self.start)*i/(numValues-1) # corresponding to that value of i
        else:
            if isnan(self.default): # if default is NaN, return start
                return self.start
            else:
                return self.default

    def __add__(self, other):
        return Parameter(start=self.start + other, stop = self.stop + other,
            default = self.default + other, includeFinal = self.includeFinal)

    def __radd__(self, other):
        return self.__add__(other)

    def __sub__(self, other):
        return Parameter(start=self.start - other, stop = self.stop - other,
            default = self.default - other, includeFinal = self.includeFinal)
    
    def __rsub__(self, other):
        return Parameter(start=other - self.start, stop = other - self.stop,
            default = other - self.default, includeFinal = self.includeFinal)

    def __mul__(self, other):
        return Parameter(start=other * self.start, stop = other * self.stop,
            default = other*self.default, includeFinal = self.includeFinal)
    
    def __rmul__(self, other):
        return self.__mul__(other)

    def __neg__(self):
        return Parameter(start=-self.start, stop = -self.stop,
            default = - self.default, includeFinal = self.includeFinal)

            
