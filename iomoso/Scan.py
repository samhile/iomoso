#!/usr/bin/env
import qutip as qu
import numpy as np
import pickle
import os
from numpy import nan, isnan
from functools import partial
from sys import exit
import Core

class TimeScan:
    def __init__(self, H = None, times = None, initial = None, options = None):
        self.H = H
        self.times = times
        self.initial = initial
        self.result = None
        if options is 'fine':
            self.options = qu.Options(atol=1e-9, rtol=1e-9, nsteps=60000)
        elif isinstance(amp, qu.Options):
            self.options = options
        else:
            self.options = None

    def genHterms(self, i = None, numValues = None):   ### generates hamiltonian terms and returns a list of them
        """
        This function takes the dictionaries in the Hterms list and converts them into a 
        form that the QuTiP solver can compute.
        """
        H = []
        for Hterm in self.H.Hterms:
            if Hterm['timedep']:
                op = Hterm['op']

                amp = Hterm['amp']
                if isinstance(amp, Core.Parameter):        # if amp or det are parameter objects, use the getValue method to select 1
                    amp = amp.getValue(i, numValues)  # value in the list

                det=Hterm['det']
                if isinstance(det, Core.Parameter):
                    det = det.getValue(i, numValues)
            
                fac = Hterm['factor'](amp = amp, det = det)   # substitute parameters
                H.append([op, fac])
            else:
                H.append(Hterm['op'])        
        return H

    def run(self, i = None, numValues = None):
        """
        Use the genHterms method to get Ham terms, then call the QuTiP solver to find the solutions
        """
        H = self.genHterms(i, numValues)         ### generates hamiltonian elements and creates result object with solutions
        self.result = qu.mesolve(H, self.initial, self.times, [], [], options=self.options, progress_bar=True)

    def pickleScan(self, filename, i = None, numValues = None):
        """
        This method generates Hamiltonian terms for each parameter and pickles each one along with the
        class instance variables (such as the time(s) and the initial state) that are passed at the initialisation
        of the timescan object. This can be done in a for loop when there are lots of parameters to 
        create pickled files ready to be read and solved.
        """
        H = self.genHterms(i, numValues)
        mePars = {'H': H, 'initial':self.initial, 'times':self.times}
        file = open(filename, 'wb')   ### wb = open for writing, truncating first & binary mode
        pickle.dump(mePars, file)
        file.close()

    def unpickleResult(self, filename):
        """
        function to unpickle pickled data files.
        """
        file = open(filename, 'rb')     ### rb = open for reading & binary mode
        self.result = pickle.load(file)
        file.close()

    def getPopulations(self, state_list):
        """
        Function for finding the probabilities of an ion or ions being in a certain state. Starts by checking
        the state list it takes as an argument is equal in length to the number of ions the Hamiltonian was
        initialised with. If true, replace every item in the input list with the corresponding state from the
        states dictionary. Next, create an empty list that will store the probabilities, and then for each state
        in the result 'object' (which stores the state of the system at each timestep), create an operator (in much
        the same way as in the op_construct function) that the qu.expect function can use to measure a probability.
        lastly, append this probability to the list created earlier. 
        """
        if len(state_list) == self.H.nions:
            state_list = [ self.H.states_dict[i] for i in state_list ]  
            p = []
            for psi in self.result.states:
                p.append( qu.expect( qu.tensor(*(state_list + [self.H.Imot]))*qu.tensor(*(state_list + [self.H.Imot])).dag(), psi) )

            return np.array(p)
        else:
            raise ValueError('Number of states specified in getPopulations argument is '
                             'different to the number of ions the Hamiltonian object was '
                             'initialised with. Please ensure the states of all ions to be measured '
                             'are declared correctly.')

    def getFidelity(self, bell_state):   # needs to be changed so that it works with > 2 ions in system
        """
        Works pretty much the same way as getPopulations, except it doesnt return a list. Just compares last result
        state stored to a user-specified Bell state.
        """
        bell_state = self.H.bell_dic[bell_state] # replaces bell state argument with value from bell state dictionary
        return qu.expect(bell_state*bell_state.dag(), self.result.states[-1])



class ParamScan:
    def __init__(self, H = None, times = None, initial = None):
        self.H = H
        if isinstance(times, list): ### checks if times is a list, if true create instance variable self.times, if times is a number
            self.times = times      ### convert it to a list as below in else statement
        else:
            self.times = [0, times]
        self.initial = initial

    def run(self, numValues = None, printProgress = True):
        """
        Works pretty much the same way as the run function in the timescan object. It first creates a timescan
        object from the parameters that the paramscan object was initialised with, and then calls the timescan
        run function to solve the hamiltonian for each parameter. Results are stored in the Results list.
        """
        self.numValues = numValues
        self.results = []
        sim = TimeScan(self.H, self.times, self.initial) 
        for i in range(numValues):
            sim.run(i, numValues) 
            self.results.append(sim.result) 
            if printProgress:
                print('completed {i} of {numValues}'.format(i=i+1, numValues=numValues))

    def runHPC(self, numValues = None):
        """
        Function to be called when code is ran on the HPC. First check if logged onto HPC, then create
        pickled input files for all inputs. Then edit job script and submit it to scheduling engine,
        which sends each pickled input with the integrateHamHPC.py script to its own node to run. This
        script then pickles the output and writes it to a new data file with a name corresponding to the 
        input file. 
        """
        # print(os.getcwd())
        if 'apollo' in os.uname()[1]:
            self.numValues = numValues
            sim = TimeScan(self.H, self.times, self.initial)

            for i in range(numValues):
                sim.pickleScan('mepars_{i}.pickle'.format(i=i+1), i = i, numValues = numValues)

            lines = open('generic_job_script.job', 'r').readlines()
            lines[1] = '#$ -t 1-{no_jobs}\n'.format(no_jobs = numValues)
            out = open('generic_job_script.job', 'w')
            out.writelines(lines)
            out.close()

            os.system('qsub generic_job_script.job')
            exit()
        else:
            print('\nYou are not currently logged into the HPC, so the runHPC function is disabled. If '
                  'you have data from the HPC to analyse, you can call the datafile analysis function.\n')

    def getPopulations(self, state_list):
        if len(state_list) == self.H.nions:
            state_list = [ self.H.states_dict[i] for i in state_list ]
            p = []
            for result in self.results:
                p.append(qu.expect(qu.tensor(*(state_list+[self.H.Imot]))*qu.tensor(*(state_list+[self.H.Imot])).dag(), result.states[-1]))

            return np.array(p)
        else:
            print('\n','Error: Number of states specified in getPopulations argument is '
                  'different to the number of ions the Hamiltonian object was '
                  'initialised with. Please ensure the states of all ions to be measured '
                  'are declared correctly.','\n')
            exit()

    def getFidelity(self, bell_state):
        bell_state = self.H.bell_dic[bell_state]
        return qu.expect(bell_state*bell_state.dag(), self.results.states[-1])


