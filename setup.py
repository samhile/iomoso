#!/usr/bin/env python3

import sys
from setuptools import setup
from setuptools import find_packages


if sys.version_info[:3] < (3, 5):
    raise SystemExit("You need Python 3.5+")


setup(
    name="iomoso",
    version="0.2.dev",
    description="a qutip based package for trapped ion molmer sorenson gate simulations",
    long_description=open("README.txt").read(),
    author="Chris Knapp, Sam Hile, Simon Webster",
    author_email="samhile@gmail.com",
    license="BSD-3-Clause-Clear",
    platforms=["Any"],
    keywords="qutip quantum simulation ion",
    classifiers=[
        "Operating System :: OS Independent",
        "Programming Language :: Python",
    ],
    packages=find_packages(),
    install_requires=["qutip", "numpy"],
    include_package_data=True,
)
